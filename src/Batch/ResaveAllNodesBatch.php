<?php

namespace Drupal\resave_all_nodes\Batch;

use Drupal\node\Entity\Node;

/**
 * Resave All Nodes batch.
 *
 *   See
 * https://git.drupalcode.org/project/drupal/blob/8.7.8/core/includes/form.inc#L556-668
 * for detailed information on batch processing.
 *
 * @package Drupal\resave_all_nodes\Batch
 */
class ResaveAllNodesBatch {

  /**
   * Resave nodes.
   *
   * @param array $chunk
   *   An array of node IDs.
   * @param array $context
   *   A batch context array.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function batchOperation(array $chunk, array &$context) {
    $nodes = Node::loadMultiple($chunk);
    /** @var \Drupal\node\Entity\Node $node */
    foreach ($nodes as $node) {
      $node->save();
      // Make sure node is translatable.
      if ($node->isTranslatable()) {
        // Get node translations and exclude default.
        foreach ($node->getTranslationLanguages(FALSE) as $langcode => $language) {
          // Make sure translations exists.
          if ($node->hasTranslation($langcode)) {
            // Save translation.
            $node->getTranslation($langcode)->save();
          }
        }
      }
      $context['results'][] = $node->id();
    }
  }

  /**
   * Handle batch completion.
   *
   * @param bool $success
   *   TRUE if all batch API tasks were completed successfully.
   * @param array $results
   *   An array of resaved node IDs.
   * @param array $operations
   *   A list of the operations that had not been completed.
   */
  public static function batchFinished($success, array $results, array $operations) {
    $messenger = \Drupal::messenger();

    if ($success) {
      $messenger->addMessage(t('Resaved @count nodes.', [
        '@count' => count($results),
      ]));
    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $message = t('An error occurred while processing %error_operation with arguments: @arguments', [
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE),
      ]);
      $messenger->addError($message);
    }
  }

}

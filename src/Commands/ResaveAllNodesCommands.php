<?php

namespace Drupal\resave_all_nodes\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drush\Commands\DrushCommands;
use Drush\Utils\StringUtils;

/**
 * Provide Drush commands for the Resave All Nodes module.
 */
class ResaveAllNodesCommands extends DrushCommands {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * ResaveAllNodesCommands constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct();
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Resave all nodes command.
   *
   * @param array $options
   *   Array of options as described below.
   *
   * @option bundles
   *   A comma-delimited list of content types to resave.
   * @option chunk-size
   *   Define how many nodes will be resaved in the same step. Default: 250.
   * @usage drush resave-all-nodes
   *   Resave all nodes in chunks of 250.
   * @usage drush resave-all-nodes --bundles=article
   *   Resave all article nodes.
   * @usage drush resave-all-nodes --chunk-size=5
   *   Resave all nodes in chunks of 5.
   *
   * @command resave-all-nodes
   * @aliases ran
   */
  public function resaveAllNodes(array $options = [
    'bundles' => self::REQ,
    'chunk-size' => 250,
  ]) {
    $query = $this->getQuery($options)->accessCheck(TRUE);
    $result = $query->execute();
    if (empty($result)) {
      $this->logger()->success(dt('No matching nodes found.'));
    }
    else {
      $this->io()->progressStart(count($result));
      foreach (array_chunk($result, $options['chunk-size'], TRUE) as $chunk) {
        drush_op('\Drupal\resave_all_nodes\Batch\ResaveAllNodesBatch::batchOperation', $chunk, []);
        $this->io()->progressAdvance(count($chunk));
      }
      $this->io()->progressFinish();
      $this->logger()->success(dt("Saved node IDs: !ids", ['!ids' => implode(', ', array_values($result))]));
    }
  }

  /**
   * The query to get the nodes.
   *
   * @param array $options
   *   The options passed along the command, the get the bundles if any.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The query object that can query the given node types or all nodes.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getQuery(array $options): QueryInterface {
    $storage = $this->getEntityTypeManager()->getStorage('node');
    $query = $storage->getQuery()->accessCheck(FALSE);
    if ($bundles = StringUtils::csvToArray((string) $options['bundles'])) {
      $query = $query->condition('type', $bundles, 'IN');
    }
    return $query;
  }

  /**
   * Retrieve the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  public function getEntityTypeManager(): EntityTypeManagerInterface {
    return $this->entityTypeManager;
  }

}
